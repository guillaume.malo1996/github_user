defmodule GofetchWeb.PageController do
  use GofetchWeb, :controller

  def index(conn, %{"username" => username}) do
    query = """
    {
      user(login: "#{username}") {
        login
        watching(first: 100) {
          pageInfo{
            endCursor
            hasNextPage
          }
          nodes{
            name
            url
          }
        }
      }
    }
    """

    headers = ["Authorization": "Bearer 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", "Content-Type": "application/json"]
    resp = HTTPoison.post! "https://api.github.com/graphql", Jason.encode!(%{query: query}), headers
    user = Jason.decode!(resp.body)["data"]["user"]

    repos = user["watching"]["nodes"] ++ fetch_additional_nodes(username, user["watching"]["pageInfo"])

    render(conn, "index.html", name: user["login"], repos: repos)
  end

  def fetch_additional_nodes(username, pageInfo) do
    if pageInfo["hasNextPage"] do
      query = """
      {
        user(login: "#{username}") {
          watching(first: 100, after: "#{pageInfo["endCursor"]}") {
            pageInfo{
              endCursor
              hasNextPage
            }
            nodes{
              name
              url
            }
          }
        }
      }
      """
      headers = ["Authorization": "Bearer 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", "Content-Type": "application/json"]
      resp = HTTPoison.post! "https://api.github.com/graphql", Jason.encode!(%{query: query}), headers
      data = Jason.decode!(resp.body)["data"]["user"]["watching"]
      data["nodes"] ++ fetch_additional_nodes(username, data["pageInfo"])
    else
      []
    end
  end
end
